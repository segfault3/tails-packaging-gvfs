-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

Format: 3.0 (quilt)
Source: gvfs
Binary: gvfs, gvfs-daemons, gvfs-libs, gvfs-common, gvfs-fuse, gvfs-backends, gvfs-bin
Architecture: any all
Version: 1.30.4-1
Maintainer: Debian GNOME Maintainers <pkg-gnome-maintainers@lists.alioth.debian.org>
Uploaders: Andreas Henriksson <andreas@fatal.se>, Laurent Bigonville <bigon@debian.org>, Michael Biebl <biebl@debian.org>
Homepage: https://wiki.gnome.org/Projects/gvfs
Standards-Version: 3.9.8
Vcs-Browser: https://anonscm.debian.org/viewvc/pkg-gnome/desktop/unstable/gvfs/
Vcs-Svn: svn://anonscm.debian.org/pkg-gnome/desktop/unstable/gvfs
Testsuite: autopkgtest
Testsuite-Triggers: apache2, apache2-dev, dbus-x11, gir1.2-umockdev-1.0, python-twisted-core, python3-gi, samba, umockdev
Build-Depends: debhelper (>= 10), dh-exec (>= 0.13), gnome-pkg-tools (>= 0.7), pkg-config, gtk-doc-tools, libcap-dev [linux-any], libglib2.0-dev (>= 2.49.3), libdbus-1-dev, openssh-client, libsoup2.4-dev (>= 2.42.0), libxml2-dev, libudev-dev (>= 0.138) [linux-any], libavahi-glib-dev (>= 0.6), libavahi-client-dev (>= 0.6), libfuse-dev [!hurd-any], libgudev-1.0-dev (>= 147) [linux-any], libcdio-paranoia-dev (>= 0.78.2), libbluetooth-dev (>= 4.0) [linux-any], libgoa-1.0-dev (>= 3.17.1), libgdata-dev (>= 0.17.3), libexpat1-dev, libgphoto2-dev (>= 2.5.0), libsecret-1-dev, libbluray-dev, libmtp-dev (>= 1.1.6), libpolkit-gobject-1-dev, libsmbclient-dev (>= 3.4.0) [!hurd-any], libarchive-dev, libgcrypt20-dev (>= 1.2.2), libltdl-dev, libimobiledevice-dev (>= 1.2) [!hurd-any], libplist-dev, libudisks2-dev (>= 1.97) [linux-any], libsystemd-dev [linux-any], systemd (>= 206) [linux-any], libgcr-3-dev, libgtk-3-dev, libnfs-dev (>= 1.9.7)
Package-List:
 gvfs deb libs optional arch=any
 gvfs-backends deb gnome optional arch=any
 gvfs-bin deb gnome optional arch=any
 gvfs-common deb libs optional arch=all
 gvfs-daemons deb libs optional arch=any
 gvfs-fuse deb gnome optional arch=linux-any,kfreebsd-any
 gvfs-libs deb libs optional arch=any
Checksums-Sha1:
 77a6f86b9fb83bcee308592e90af4f13983e794b 1895304 gvfs_1.30.4.orig.tar.xz
 a0310f13e78102ec5ff5473a23cfbb3e7bc237a4 19180 gvfs_1.30.4-1.debian.tar.xz
Checksums-Sha256:
 981e0aca7f4e2e99860137f9fd99c335fa72a764156d253caf1069380a8e3afa 1895304 gvfs_1.30.4.orig.tar.xz
 4c8d2af3ef2c0a62b933d3cb8cb6b9f56aab2265ea1c3477a2c2f0b7665c3f21 19180 gvfs_1.30.4-1.debian.tar.xz
Files:
 4774661e1a4982a0476ba0f30c52050c 1895304 gvfs_1.30.4.orig.tar.xz
 c1e66718b5b68c35185a9f7ffa6bb2ab 19180 gvfs_1.30.4-1.debian.tar.xz

-----BEGIN PGP SIGNATURE-----

iQJFBAEBCAAvFiEECbOsLssWnJBDRcxUauHfDWCPItwFAljbAM8RHGJpZWJsQGRl
Ymlhbi5vcmcACgkQauHfDWCPItwmNw/+IQmIuA0KC1gS0GDOU8cWIehAVLgzXDIa
sBIgA1Rhk/gY2Wl0/yq/C3RZ8ra45mOnQV6+trTiAiwpM+i4ZN7QRTGPBVQTqKPJ
LBP5cmxV8sM/fyGI8AoAQ5edJv3TSdZbCN6PKFHMhPXQF9Z379dARoZBK6HiNyNa
jJG8dv3/oG9gkaD4PHwg+FXRJZj9/P0qroZgglUS57tsHAFrai5aWuhgUePw0N4s
h4UCe4OvUsWWYFyzLotY37z522A01JwCF3Ux9NED6mbHTJEwlgMiABvmDXxnJ6qq
xSZGvvLqZ1x6qYaFGS/c6t9W+FRhuawWJ0BpRPvDUreEJU4lXSKR/GEwzUKjqGmu
vcQONfVhGCyX5UQuMMLFd5yBq438VoAhg7bC33XM6T45n6Baw2nTI6iW8huh99My
6K/lmyTW0iAm8hF1aKXEVJsL+aR+rXp2ZQpngzreqtTSehIZLnDHQJuD2mgGiKJ8
ziJe0XBCwLC5AvHE10qPg0RJljYMpjshUJ72Pz0ofQXFNcMhz/kK1JAVwJdshEzm
OpWEnuK7BIOjGMYHLBwokPSsoHZJmO1dRLXtfmyjS3Pdd7nBfR4aEg9R0x2W7K2m
droxelp0nRDn2kNNzmyW8F4vj2E5Zw2RKlLSK+nyqYezbzLlC6woMbI062v8rUSX
Yy0Ne+nBzNA=
=nn4B
-----END PGP SIGNATURE-----
